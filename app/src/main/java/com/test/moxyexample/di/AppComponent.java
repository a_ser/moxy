package com.test.moxyexample.di;

import android.content.Context;
import com.test.moxyexample.CoreApplication;

import com.test.moxyexample.utils.SchedulerProvider;
import dagger.Component;
import retrofit2.Retrofit;

import javax.inject.Singleton;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/26/17.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(CoreApplication application);

    Retrofit provideRetrofit();

    Context providesContext();

    SchedulerProvider providesScheduler();
}
