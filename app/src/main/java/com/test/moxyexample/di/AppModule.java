package com.test.moxyexample.di;

import javax.inject.Singleton;

import com.test.moxyexample.CoreApplication;
import com.test.moxyexample.utils.MainSchedulerProvider;
import com.test.moxyexample.utils.SchedulerProvider;

import android.content.Context;
import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/26/17.
 */
@Module
public class AppModule {
    private final CoreApplication mApplication;

    public AppModule(@NonNull final CoreApplication application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mApplication.getApplicationContext();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(@NonNull final Converter.Factory factory, @NonNull final String baseUrl) {
        return new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(factory).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();
    }

    @Provides
    @Singleton
    Converter.Factory providesFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @Singleton
    String provideBaseUrl() {
        return "http://backend1.lordsandknights.com/XYRALITY/WebObjects/BKLoginServer.woa/wa/";
    }

    @Provides
    @Singleton
    SchedulerProvider schedulerProvider() {
        return new MainSchedulerProvider();
    }
}
