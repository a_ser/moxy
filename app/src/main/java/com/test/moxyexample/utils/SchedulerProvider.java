package com.test.moxyexample.utils;

import io.reactivex.Scheduler;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/27/17.
 */
public interface SchedulerProvider {
    Scheduler ui();

    Scheduler computation();

    Scheduler io();
}
