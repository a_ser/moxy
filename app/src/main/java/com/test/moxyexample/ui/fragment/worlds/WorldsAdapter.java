package com.test.moxyexample.ui.fragment.worlds;

import com.test.moxyexample.R;
import com.test.moxyexample.domain.worlds.World;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/26/17.
 */
public class WorldsAdapter extends RecyclerView.Adapter<WorldsAdapter.WorldsViewHolder> {
    private IProvider<World> mProvider;

    WorldsAdapter(final IProvider<World> provider) {
        mProvider = provider;
    }

    @Override
    public WorldsViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new WorldsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.world_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final WorldsViewHolder holder, final int position) {
        final World world = mProvider.get(position);
        holder.worldName.setText(world.getName());
        holder.country.setText(world.getCountry());
        holder.description.setText(world.getWorldStatus().getDescription());
    }

    @Override
    public int getItemCount() {
        return mProvider.getItemCount();
    }

    static class WorldsViewHolder extends RecyclerView.ViewHolder {
        TextView description;
        TextView worldName;
        TextView country;

        WorldsViewHolder(final View itemView) {
            super(itemView);
            worldName = (TextView) itemView.findViewById(R.id.tv_world_name);
            country = (TextView) itemView.findViewById(R.id.tv_country);
            description = (TextView) itemView.findViewById(R.id.tv_description);
        }
    }
}
