package com.test.moxyexample.ui.base;

import com.test.moxyexample.R;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/27/17.
 */
public class LoadingFragment extends Fragment {
    public static final String TAG = "LoadingFragment";

    public static Fragment create() {
        return new LoadingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_loader, container, false);
    }
}
