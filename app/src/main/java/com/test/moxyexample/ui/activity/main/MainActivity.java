package com.test.moxyexample.ui.activity.main;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.test.moxyexample.R;
import com.test.moxyexample.presentation.presenter.main.MainPresenter;
import com.test.moxyexample.presentation.view.main.MainView;
import com.test.moxyexample.ui.activity.dummy.DummyActivity;
import com.test.moxyexample.ui.fragment.login.LoginFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class MainActivity extends MvpAppCompatActivity implements MainView {
    public static final String                                  TAG = "MainActivity";
    @InjectPresenter(type = PresenterType.GLOBAL) MainPresenter mMainPresenter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            navigateToLoginScreen();
        }
    }

    public void navigateToFragment(final Fragment fragment, final String tag) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, tag).commit();
    }

    @Override
    public void navigateToLoginScreen() {
        navigateToFragment(LoginFragment.newInstance(), LoginFragment.TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // This is important : Hack to open a dummy activity for 200-500ms (cannot be noticed by user as it is for 500ms
        // and transparent floating activity and auto finishes)
        startActivity(new Intent(this, DummyActivity.class));
        finish();
    }
}
