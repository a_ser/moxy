package com.test.moxyexample.ui.fragment.worlds.di;

import com.test.moxyexample.domain.worlds.WebService;
import com.test.moxyexample.presentation.presenter.worlds.WorldsPresenter;

import android.support.annotation.NonNull;
import com.test.moxyexample.utils.SchedulerProvider;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/26/17.
 */
@Module
class WorldsModule {
    @Provides
    WorldsPresenter providesPresenter(final WebService webService, @NonNull final SchedulerProvider schedulerProvider) {
        return new WorldsPresenter(webService, schedulerProvider);
    }

    @Provides
    WebService providesWebService(@NonNull final Retrofit retrofit) {
        return retrofit.create(WebService.class);
    }
}
