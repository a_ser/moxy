package com.test.moxyexample.ui.fragment.worlds.di;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/26/17.
 */
@Scope
@Retention(RetentionPolicy.SOURCE)
@interface WorldsScope {}
