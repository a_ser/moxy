package com.test.moxyexample.ui.activity.dummy;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

/**
 * Android's Memory Leak with
 */
public class DummyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EditText editText = new EditText(this);
        setContentView(editText);
        editText.clearFocus();
        editText.setTextIsSelectable(false);
        new Handler().postDelayed(this::finish, 500);
    }
}
