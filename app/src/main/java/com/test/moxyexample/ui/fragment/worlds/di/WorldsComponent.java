package com.test.moxyexample.ui.fragment.worlds.di;

import com.test.moxyexample.di.AppComponent;
import com.test.moxyexample.ui.fragment.worlds.WorldsFragment;

import dagger.Component;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/26/17.
 */
@WorldsScope
@Component(dependencies = AppComponent.class, modules = WorldsModule.class)
public interface WorldsComponent {
    void inject(WorldsFragment fragment);
}
