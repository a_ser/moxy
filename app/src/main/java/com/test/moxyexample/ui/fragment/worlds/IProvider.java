package com.test.moxyexample.ui.fragment.worlds;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/26/17.
 */
public interface IProvider<T> {
    T get(int position);

    int getItemCount();
}
