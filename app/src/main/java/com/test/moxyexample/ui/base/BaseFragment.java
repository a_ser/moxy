package com.test.moxyexample.ui.base;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.test.moxyexample.ui.activity.main.MainActivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/26/17.
 */
public class BaseFragment extends MvpAppCompatFragment {
    protected void navigateFragment(final Fragment fragment, final String tag) {
        ((MainActivity) getActivity()).navigateToFragment(fragment, tag);
    }

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getTitle() != null) {
            getActivity().setTitle(getTitle());
        }
    }

    @Nullable
    public String getTitle() {
        return null;
    }
}
