package com.test.moxyexample.ui.fragment.login;

import android.support.annotation.Nullable;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.test.moxyexample.R;
import com.test.moxyexample.presentation.presenter.login.LoginPresenter;
import com.test.moxyexample.presentation.view.login.LoginView;
import com.test.moxyexample.ui.base.BaseFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.test.moxyexample.ui.fragment.worlds.WorldsFragment;

public class LoginFragment extends BaseFragment implements LoginView {
    public static final String      TAG = "LoginFragment";
    @InjectPresenter LoginPresenter mLoginPresenter;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_login, container, false);
        final EditText emailEt = (EditText) view.findViewById(R.id.email);
        final EditText passwordEt = (EditText) view.findViewById(R.id.password);
        view.findViewById(R.id.send).setOnClickListener(v -> mLoginPresenter.validateAccount(emailEt.getText().toString(), passwordEt.getText().toString()));
        return view;
    }

    @Override
    public void navigateToWorldsScreen(final String email, final String password) {
        navigateFragment(WorldsFragment.newInstance(email, password), WorldsFragment.TAG);
    }

    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.login);
    }
}
