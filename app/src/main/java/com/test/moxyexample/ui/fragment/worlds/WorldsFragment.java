package com.test.moxyexample.ui.fragment.worlds;

import javax.inject.Inject;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.test.moxyexample.CoreApplication;
import com.test.moxyexample.R;
import com.test.moxyexample.domain.worlds.World;
import com.test.moxyexample.presentation.presenter.worlds.WorldsPresenter;
import com.test.moxyexample.presentation.view.worlds.WorldsView;
import com.test.moxyexample.ui.base.BaseFragment;
import com.test.moxyexample.ui.base.LoadingFragment;
import com.test.moxyexample.ui.fragment.worlds.di.DaggerWorldsComponent;
import com.test.moxyexample.ui.fragment.worlds.di.WorldsComponent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class WorldsFragment extends BaseFragment implements WorldsView {
    public static final String               TAG            = "WorldsFragment";
    private static final String              EMAIL_EXTRA    = "email";
    private static final String              PASSWORD_EXTRA = "password";
    private WorldsComponent                  mComponent;
    private RecyclerView                     mRecyclerView;
    @Inject @InjectPresenter WorldsPresenter presenter;

    @ProvidePresenter
    WorldsPresenter providePresenter() {
        return presenter;
    }

    public static WorldsFragment newInstance(final String email, final String password) {
        final WorldsFragment fragment = new WorldsFragment();
        final Bundle args = new Bundle(2);
        args.putString(EMAIL_EXTRA, email);
        args.putString(PASSWORD_EXTRA, password);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        getComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    private WorldsComponent getComponent() {
        if (mComponent == null) {
            mComponent = DaggerWorldsComponent.builder().appComponent(((CoreApplication) getActivity().getApplicationContext()).getComponent()).build();
        }
        return mComponent;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_worlds, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_worlds);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.loadWorlds(getArguments().getString(EMAIL_EXTRA), getArguments().getString(PASSWORD_EXTRA));
    }

    @Override
    public void setAdapter(final IProvider<World> provider) {
        mRecyclerView.setAdapter(new WorldsAdapter(provider));
    }

    @Override
    public void notifyDataSetChanged() {
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showError(final Throwable throwable) {
        throwable.printStackTrace();
        Snackbar.make(getView(), R.string.error_happened, Snackbar.LENGTH_SHORT).show();
        hideLoader();
    }

    @Nullable
    @Override
    public String getTitle() {
        return getString(R.string.worlds);
    }

    @Override
    public void showLoader() {
        final Fragment loadingFragment = getChildFragmentManager().findFragmentByTag(LoadingFragment.TAG);
        if (loadingFragment == null) {
            getChildFragmentManager().beginTransaction().add(R.id.worlds_container, LoadingFragment.create(), LoadingFragment.TAG).commitNow();
        }
    }

    @Override
    public void hideLoader() {
        final Fragment loadingFragment = getChildFragmentManager().findFragmentByTag(LoadingFragment.TAG);
        if (loadingFragment != null) {
            getChildFragmentManager().beginTransaction().remove(loadingFragment).commitNow();
        }
    }
}
