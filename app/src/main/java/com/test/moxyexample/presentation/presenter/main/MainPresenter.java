package com.test.moxyexample.presentation.presenter.main;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.test.moxyexample.presentation.view.main.MainView;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {}
