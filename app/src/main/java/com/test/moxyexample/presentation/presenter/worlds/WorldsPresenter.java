package com.test.moxyexample.presentation.presenter.worlds;

import java.util.List;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.test.moxyexample.domain.worlds.WebService;
import com.test.moxyexample.domain.worlds.World;
import com.test.moxyexample.presentation.view.worlds.WorldsView;
import com.test.moxyexample.ui.fragment.worlds.IProvider;
import com.test.moxyexample.utils.SchedulerProvider;

import android.os.Build;
import android.support.annotation.NonNull;
import io.reactivex.disposables.Disposable;

@InjectViewState
public class WorldsPresenter extends MvpPresenter<WorldsView> implements IProvider<World> {
    private final WebService                 mWorldsService;
    @NonNull private final SchedulerProvider mSchedulerProvider;
    private Disposable                       mDisposable;
    private List<World>                      mWorlds;

    public WorldsPresenter(@NonNull final WebService worldsService, @NonNull final SchedulerProvider schedulerProvider) {
        mWorldsService = worldsService;
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void attachView(final WorldsView view) {
        super.attachView(view);
        view.setAdapter(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    public void loadWorlds(final String email, final String password) {
        if (mWorlds == null) {
            // @formatter:off
           mDisposable = mWorldsService
                    .retrieveWorlds(email, password, getDeviceType(), getDeviceId())
                    .subscribeOn(mSchedulerProvider.io())
                    .observeOn(mSchedulerProvider.ui())
                    .doOnSubscribe(disposable -> getViewState().showLoader())
                    .doOnComplete(() -> getViewState().hideLoader())
                    .doOnError(throwable -> getViewState().showError(throwable))
                    .subscribe(gameWorldsResponse -> {
                        mWorlds = gameWorldsResponse.getWorlds();
                        getViewState().notifyDataSetChanged();
                    }, throwable -> getViewState().showError(throwable));

            // @formatter:on
        } else {
            getViewState().notifyDataSetChanged();
        }
    }

    private String getDeviceType() {
        return String.format("%s %s", Build.MODEL, Build.VERSION.RELEASE);
    }

    private String getDeviceId() {
        return "02:00:00:00:00:00";
    }

    @Override
    public void onDestroy() {
        if (mDisposable != null && !mDisposable.isDisposed()) {
            mDisposable.dispose();
        }
        super.onDestroy();
    }

    @Override
    public World get(final int position) {
        return mWorlds.get(position);
    }

    @Override
    public int getItemCount() {
        return mWorlds == null ? 0 : mWorlds.size();
    }
}
