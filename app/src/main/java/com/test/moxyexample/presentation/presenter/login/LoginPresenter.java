package com.test.moxyexample.presentation.presenter.login;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.test.moxyexample.presentation.view.login.LoginView;

@InjectViewState
public class LoginPresenter extends MvpPresenter<LoginView> {

    public void validateAccount(final String email, final String pasword) {
        // TODO add some validation stuff
        getViewState().navigateToWorldsScreen(email, pasword);
    }
}
