package com.test.moxyexample.presentation.view.login;

import com.arellomobile.mvp.MvpView;

public interface LoginView extends MvpView {
    void navigateToWorldsScreen(String email, String password);
}
