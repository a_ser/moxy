package com.test.moxyexample.presentation.view.worlds;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.test.moxyexample.domain.worlds.World;
import com.test.moxyexample.ui.fragment.worlds.IProvider;

public interface WorldsView extends MvpView {
    void setAdapter(IProvider<World> provider);

    @StateStrategyType(SkipStrategy.class)
    void notifyDataSetChanged();

    @StateStrategyType(SkipStrategy.class)
    void showError(Throwable throwable);

    void showLoader();

    void hideLoader();
}
