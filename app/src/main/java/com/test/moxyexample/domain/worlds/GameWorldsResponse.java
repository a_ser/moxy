package com.test.moxyexample.domain.worlds;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class GameWorldsResponse {

    private String                                                 serverVersion;
    @SerializedName("allAvailableWorlds") private ArrayList<World> worlds;

    public String getServerVersion() {
        return serverVersion;
    }

    public void setServerVersion(final String serverVersion) {
        this.serverVersion = serverVersion;
    }

    public ArrayList<World> getWorlds() {
        return worlds;
    }

    public void setWorlds(final ArrayList<World> worlds) {
        this.worlds = worlds;
    }
}
