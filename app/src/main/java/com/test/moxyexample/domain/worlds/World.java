package com.test.moxyexample.domain.worlds;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * author tetiana
 */
public class World implements Parcelable {

    private int id;
    private String language;
    private String country;
    private String url;
    private String mapUrl;
    private String name;
    private WorldStatus worldStatus;

    protected World(Parcel in) {
        id = in.readInt();
        language = in.readString();
        country = in.readString();
        url = in.readString();
        mapUrl = in.readString();
        name = in.readString();
        worldStatus = in.readParcelable(WorldStatus.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(language);
        dest.writeString(country);
        dest.writeString(url);
        dest.writeString(mapUrl);
        dest.writeString(name);
        dest.writeParcelable(worldStatus, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<World> CREATOR = new Creator<World>() {
        @Override
        public World createFromParcel(Parcel in) {
            return new World(in);
        }

        @Override
        public World[] newArray(int size) {
            return new World[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMapUrl() {
        return mapUrl;
    }

    public void setMapUrl(String mapUrl) {
        this.mapUrl = mapUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WorldStatus getWorldStatus() {
        return worldStatus;
    }

    public void setWorldStatus(WorldStatus worldStatus) {
        this.worldStatus = worldStatus;
    }

    public static class WorldStatus implements Parcelable {

        private int id;
        private String description;

        protected WorldStatus(Parcel in) {
            id = in.readInt();
            description = in.readString();
        }

        public static final Creator<WorldStatus> CREATOR = new Creator<WorldStatus>() {
            @Override
            public WorldStatus createFromParcel(Parcel in) {
                return new WorldStatus(in);
            }

            @Override
            public WorldStatus[] newArray(int size) {
                return new WorldStatus[size];
            }
        };

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(description);
        }
    }

}
