package com.test.moxyexample.domain.worlds;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface WebService {

    @Headers({"Accept: application/json"})
    @POST("worlds")
    @FormUrlEncoded
    Observable<GameWorldsResponse> retrieveWorlds(@Field("login") String login,
                                                  @Field("password") String password,
                                                  @Field("deviceType") String deviceType,
                                                  @Field("deviceId") String deviceId);

}
