package com.test.moxyexample;

import android.content.Context;
import android.support.multidex.MultiDex;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.test.moxyexample.di.AppComponent;
import com.test.moxyexample.di.AppModule;
import com.test.moxyexample.di.DaggerAppComponent;

import android.app.Application;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/26/17.
 */
public class CoreApplication extends Application {
    AppComponent mAppComponent;

    @Override
    protected void attachBaseContext(final Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getComponent().inject(this);
        setupLeakCanary();
    }

    public AppComponent getComponent() {
        if (mAppComponent == null) {
            mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        }
        return mAppComponent;
    }

    protected RefWatcher setupLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return RefWatcher.DISABLED;
        }
        return LeakCanary.install(this);
    }
}
