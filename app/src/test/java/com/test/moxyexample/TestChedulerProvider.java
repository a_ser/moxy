package com.test.moxyexample;

import com.test.moxyexample.utils.SchedulerProvider;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/27/17.
 */
public class TestChedulerProvider implements SchedulerProvider {
    @Override
    public Scheduler ui() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler computation() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler io() {
        return Schedulers.trampoline();
    }
}
