package com.test.moxyexample.presentation.presenter.worlds;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import org.junit.Test;

import com.test.moxyexample.TestChedulerProvider;
import com.test.moxyexample.domain.worlds.GameWorldsResponse;
import com.test.moxyexample.domain.worlds.WebService;
import com.test.moxyexample.presentation.view.worlds.WorldsView;
import com.test.moxyexample.ui.fragment.worlds.IProvider;

import io.reactivex.Observable;

/**
 * <br>
 * XYRALITY GmbH 2017, moxy
 *
 * @author Andrii Seredenko
 * @since 8/27/17.
 */
public class WorldsPresenterTest {

    @Test
    public void testLoadWorldsShouldNotifyDataSetChangedIfDataIsLoaded() {
        final WebService webServiceMock = mock(WebService.class);
        final WorldsView view = mock(WorldsView.class);
        final WorldsPresenter presenter = new WorldsPresenter(webServiceMock, new TestChedulerProvider());

        presenter.attachView(view);
        when(webServiceMock.retrieveWorlds(anyString(), anyString(), anyString(), anyString())).thenReturn(Observable.fromArray(mock(GameWorldsResponse.class)));
        presenter.loadWorlds("test@test.com", "password");
        verify(view, times(1)).setAdapter(any(IProvider.class));
        verify(view, times(1)).showLoader();
        verify(view, times(1)).notifyDataSetChanged();
        verify(view, times(1)).hideLoader();
        verifyNoMoreInteractions(view);
    }
}
